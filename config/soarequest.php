<?php

/**
 * Return default client to use for requests
 * Available options include
 * 1. guzzle
 * 2. ixudra
 */

return [
    "client" => "ixudra",
    "base64_body" => false,
    "service_to_service" => true,

    /**
     * Set service common here. Pass the full class name
     * Eg: To set the service common for payroll, assuming the class is under the App\Helpers namespace.
     * 'service_common' => "App\Helpers\PayrollCommon"
     */
    "max_request_limit" => env("SOA_MAX_REQUEST_LIMIT"),
    "external_request_timeout" => env("EXTERNAL_REQUEST_TIMEOUT", 30000)
];