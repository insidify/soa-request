<?php

namespace SeamlessHr\SoaRequest;

use Illuminate\Support\ServiceProvider;
use SeamlessHr\SoaRequest\Clients\SoaGuzzle;
use SeamlessHr\SoaRequest\Helpers\SoaCommon;
use SeamlessHr\SoaRequest\Clients\SoaRequest;
use SeamlessHr\SoaRequest\Contracts\ClientInterface;
use SeamlessHr\SoaRequest\Middlewares\DecodeRequest;

class SoaRequestServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'seamlesshr');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'seamlesshr');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
        $this->app['router']->aliasMiddleware('decode-request' , DecodeRequest::class);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/soarequest.php', 'soarequest');

        $this->registerClient();
    }
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/soarequest.php' => config_path('soarequest.php'),
        ], 'soarequest.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/seamlesshr'),
        ], 'soarequest.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/seamlesshr'),
        ], 'soarequest.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/seamlesshr'),
        ], 'soarequest.views');*/

        // Registering package commands.
        // $this->commands([]);
    }

    /**
     * Register Client Interface
     */
    protected function registerClient()
    {
        $client = config("soarequest.client");
        $this->app->singleton(ClientInterface::class, function ($app) use ($client){
            return $client == "guzzle"
                    ? new SoaGuzzle(new \GuzzleHttp\Client)
                    : new SoaRequest;
        });
    }
}
