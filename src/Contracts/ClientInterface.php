<?php

namespace SeamlessHr\SoaRequest\Contracts;

interface ClientInterface
{
    /**
     * Perform a post request
     * @param string $url
     * @param mixed $request
     * @param array $headers
     */
    public function postRequest($url, $request, $headers = []);

    /**
     * Perform a get request
     * @param string $url
     * @param array $headers
     */
    public function getRequest($url, $headers = []);

    /**
     * Perform a patch request
     * @param string $url
     * @param array $requestBody
     * @param array $headers
     */
    public function patchRequest($url, $requestBody, $headers = []);

    /**
     * Perform an external patch request without adding base url
     * @param $url
     * @param $requestBody
     * @param $headers
     */
    public function externalPatchRequest($url, $request, $headers = []);

    /**
     * Perform an external post request without adding base url
     * @param $url
     * @param $requestBody
     * @param $headers
     */
    public function externalPostRequest($url, $request, $headers = [], $file = []);

    /**
     * Perform an external get request without adding base url
     * @param $url
     * @param $requestBody
     * @param $headers
     */
    public function externalGetRequest($url, $headers = []);

    public function uploadFile($url, $filePath, $extension, $title, $header, $mime = null);
}