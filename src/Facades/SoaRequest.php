<?php

namespace SeamlessHr\SoaRequest\Facades;

use Illuminate\Support\Facades\Facade;
use SeamlessHr\SoaRequest\Contracts\ClientInterface;

class SoaRequest extends Facade
{
    /**
     * Get the registered name of the component.
     * @see \SeamlessHr\SoaRequest\Clients\SoaRequest
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return ClientInterface::class;
    }
}
