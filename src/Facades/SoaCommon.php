<?php

namespace SeamlessHr\SoaRequest\Facades;

use Illuminate\Support\Facades\Facade;
use SeamlessHr\SoaRequest\Helpers\SoaCommon as Common;

class SoaCommon extends Facade
{
    /**
     * Get the registered name of the component.
     * @see \SeamlessHr\SoaRequest\Helpers\SoaCommon
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return Common::class;
    }
}
