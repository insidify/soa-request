<?php

namespace SeamlessHr\SoaRequest\Traits;

use Illuminate\Support\Facades\Cache;
use SeamlessHr\SoaRequest\Facades\SoaRequest;
use SeamlessHr\SoaRequest\Helpers\UrlConstants;

trait Appendable
{
    protected $headers = [];

    /**
     * Append Inner curl to url
     * Append Service url to
     * @param string $url
     *
     * @return $url
     */
    protected $base64_body = false;

    /**
     * Send Bas64Response
     */
    protected $base64_response = false;

    public function setBase64()
    {
        $this->base64_body = true;
    }

    public function sendBase64Response()
    {
        $this->base64_response = true;
    }

    public function dontBase64Response()
    {
        $this->base64_response = false;
    }

    public function decodeBase64Response($response)
    {
        if (($data = optional($response)->data) && is_string($data) && $this->hasBase64Response()) {
            $response->data = json_decode(base64_decode($data));
            $this->dontBase64Response();
        }
        return $response;
    }

    public function appendToUrl(string $url)
    {
        $url = config("soarequest.service_to_service", false)
            ? $this->getServiceUrl($url) . $url
            : config("soautils.esb_service") . $url;

        $has_query_params = strpos($url, "?");
        $url = $has_query_params ? $url . "&inner_curl=true" : $url . "?inner_curl=true";
        $url = $this->shouldBase64() ? $url . '&base64_body=true' : $url;
        return $this->hasBase64Response() ? $url . '&base64_response=true' : $url;
    }


    /**
     * Get service name from url
     * @param string url
     *
     * @return string $service_name
     */
    protected function getServiceFromUrl($url)
    {
        return explode("/", trim($url, "/"))[2];
    }

    /**
     * Get Version from url
     * @param string $url
     *
     * @return string $version
     */

    /**
     * Encode body to base64
     * @param array $body
     *
     * @return array $body
     */
    protected function prepareBody(array $body)
    {
        if (config("soarequest.base64_body", false)) {
            $encoder = Encoder::factory($body);
            return ["load" => $encoder()];
        }
        return $body;
    }

    /**
     * Get version from Url
     * @param string $url
     */
    protected function getVersionFromUrl(string $url)
    {
        return explode("/", trim($url, "/"))[1];
    }

    /**
     * Get service url from local cache. If not in local cache, get service url from central cache.
     * If not in central cache, get from esb and save to cache.
     * @param string $url
     * @param int $step
     *
     * @return string $url
     */
    protected function getServiceUrl(string $url, $step = 0)
    {
        return $this->getServiceUrlFromLocalCache($url) ?? $this->getServiceUrlFromCentralCache($url);
    }

    /**
     * Get Service Url From local Cache
     * @param string $url
     * @return string|null
     *
     */
    protected function getServiceUrlFromLocalCache(string $url)
    {
        return Cache::get($this->getServiceUrlCacheKey($url));
    }

    /**
     * Get Service Url From local Cache
     * @param string $url
     * @return string|null
     *
     */
    protected function getServiceUrlFromCentralCache(string $url)
    {
        $response_load = SoaRequest::externalGetRequest(
            config("soautils.esb_service") . UrlConstants::GET_SERVICE_CACHE,
             ["company-id: " . request()->header("company-id")]);

        if ($serviceUrls = optional($response_load)->data) {
            $serviceUrls = (array)$serviceUrls;
            $this->setServiceUrlToLocalCache($serviceUrls);
            return $serviceUrls[$this->getServiceUrlCacheKey($url)];
        }
        return config("soautils.esb_service");
    }

    /**
     * Get Service cache key
     */
    protected function getServiceUrlCacheKey(string $url)
    {
        return $this->getServiceFromUrl($url) . "-" . $this->getVersionFromUrl($url);
    }

    /**
     * Set Service Url to local cache
     * @param array $serviceurls
     * @return bool
     */
    protected function setServiceUrlToLocalCache(array $serviceUrls)
    {
        collect($serviceUrls)->map(function ($url, $key) {
            Cache::put($key, $url);
        });
    }

    private function checkIfBase64($response)
    {
        $response = $this->shouldBase64() ? base64_decode($response) : $response;
        $this->base64_body = config('soarequest.base64_body');
        return $this->shouldBase64() ? base64_decode($response) : $response;
    }

    private function shouldBase64()
    {
        return ($this->base64_body  || config("soarequest.base64_body", false));
    }

    public function hasBase64Response()
    {
        return $this->base64_response;
    }

    public function withBearerToken(string $token)
    {
        $this->headers['Authorization'] = 'Bearer '. $token;
        return $this;
    }

    public function acceptJson()
    {
        $this->headers['Accept'] = 'application/json';
        return $this;
    }
}
