<?php

namespace SeamlessHr\SoaRequest\Middlewares;

use SeamlessHr\SoaRequest\Helpers\Decoder;
use Closure;

class DecodeRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $decode = Decoder::factory($request->load);
        $request_body = $decode();
        foreach ($request_body as $key => $value) {
            $request->merge([$key => $value]);
        }
        return $next($request);
    }

}