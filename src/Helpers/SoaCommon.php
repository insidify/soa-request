<?php

namespace SeamlessHr\SoaRequest\Helpers;

use Illuminate\Http\Request;
use SeamlessHR\SoaUtils\SoaUtils;
use SeamlessHr\SoaRequest\Helpers\BaseCommon;
use SeamlessHR\SoaUtils\Interfaces\StatusCode;
use SeamlessHr\SoaRequest\Helpers\UrlConstants;
use SeamlessHR\SoaUtils\Exceptions\SoaException;
use SeamlessHr\SoaRequest\Contracts\ClientInterface;
use SeamlessHr\SoaRequest\Exceptions\BreezeAuthenticationException;

class SoaCommon extends BaseCommon
{
    /**
     * Get client
     */
    public function getClient()
    {
        $company = optional($this->withRelationship(["client"])->findCompany())->data;
        $client = optional($company)->client;
        if ($client){
            $is_subscribed = $client->is_subscribed;
            $is_enterprise = $client->is_enterprise;
            $enterprise_url = $client->enterprise_url;
            $client = json_decode(json_decode($client->info));
            $client->id = $company->client->id;
            $client->is_subscribed = $is_subscribed;
            $client->is_enterprise = $is_enterprise;
            $client->enterprise_url = $enterprise_url;
        }
        return  $client;
    }

    /**
     * Get Approval List
     * @param array $payload
     *
     * @return object
     */
    public function getApprovalList(array $payload)
    {
        return $this->client->postRequest(
            UrlConstants::GET_APPROVAL_LIST, $payload, $this->fillHeaders());
    }

    /**
     * Start an approval
     * @param array [$key $value]
     *
     * @return object
     */
    public function startApproval(array $requestBody)
    {
        return $this->client->postRequest(
            UrlConstants::START_APPROVAL, $requestBody, $this->fillHeaders());
    }

    /**
     * Get Approval Status
     * @param array
     *
     * @return object
     */
    public function getApprovalStatus(array $requestBody)
    {
        return $this->client->postRequest(
            UrlConstants::APPROVAL_STATUS, $requestBody, $this->fillHeaders());
    }

    /**
     * Find Company
     * @param $company_id
     * @throws SoaException
     * @return object
     */
    public function findCompany($company_id = null)
    {
        $id = $company_id ?? $this->companyId();
        if (!$id){
            throw new SoaException('No company id in header or passed', StatusCode::BAD_REQUEST);
        }
        $response = $this->client->getRequest(
            $this->appendUrl(UrlConstants::FIND_COMPANY.$id), $this->fillCompanyHeader());
        return $response;
    }

    /**
     * Delete Approval Details
     * @param array $requestBody
     *
     * @return object
     */
    public function deleteApprovalDetails(array $requestBody)
    {
        return $this->client->postRequest(
            UrlConstants::DELETE_APPROVAL, $requestBody, $this->fillHeaders());
    }

    /**
     * Delete Approval Details V2
     * @param array $requestBody
     *
     * @return object
     */
    public function deleteApprovalDetailsV2(array $requestBody)
    {
        return $this->client->postRequest(
            UrlConstants::DELETE_APPROVAL_V2, $requestBody, $this->fillHeaders());
    }

    /**
     * Search Employees
     * @param string $searchQuery
     *
     * @return object
     */
    public function searchEmployee(string $searchQuery)
    {
        return $this->client->getRequest(
            UrlConstants::SEARCH_EMPLOYEES . $searchQuery,
            $this->fillHeaders()
        );
    }

    /**
     * Get base currency of organization
     *
     * @return object
     */
    public function getCompanyBaseCurrency()
    {
        return $this->client->getRequest(UrlConstants::COMPANY_BASE_CURRENCY, $this->fillHeaders());
    }


     /**
     * Get base currency of organization
     *
     * @return object
     */
    public function getCurrencies()
    {
        return $this->client->getRequest(UrlConstants::GET_CURRENCIES, $this->fillHeaders());
    }

    /** GET BASE CURRENCY
     * @return mixed
     */
    public function getBaseCurrency()
    {
        return $this->client->getRequest(UrlConstants::GET_BASE_CURRENCIES, $this->fillHeaders());
    }

    /**
     * Get mini Employees
     * @param $requestBody
     * @return object
     */
    public function getMiniEmployees(array $requestBody)
    {
        return $this->client->postRequest(UrlConstants::MINI_EMPLOYEE, $requestBody);
    }

    /**
     * Get mini Employee
     * @param $employeeId
     * @return object
     */
    public function getMiniEmployee(int $employeeId)
    {
        return $this->client->getRequest(
            UrlConstants::MINI_EMPLOYEE . "?id=" . $employeeId, $this->fillHeaders());
    }

    /**
     * Get active Employees with basic info
     * @param $employeeIds
     * @return object
     */

    public function getActiveEmployees(array $employeeIds = [])
    {
        return $this->client->postRequest(
           UrlConstants::ACTIVE_EMPLOYEES , ['employee_ids' => $employeeIds], $this->fillHeaders());
    }
    /**
     * Submit Otp Code
     * @param $requestBody
     * @return object
     */
    public function submitOtpCode(array $requestBody)
    {
        return $this->client->postRequest(
            UrlConstants::SUBMIT_OTP_CODE, $requestBody , $this->fillHeaders());
    }

    /**
     * Get full employees
     * @param $employeeId
     *
     * @return object
     */
    public function getFullEmployee(string $employeeId)
    {
        return $this->client->getRequest(
            $this->appendUrl(UrlConstants::FULL_EMPLOYEE  . $employeeId), $this->fillHeaders());
    }


    public function getFullEmployeePost(array $employeeId)
    {
        return $this->client->postRequest(
            $this->appendUrl(UrlConstants::FULL_EMPLOYEES) , ['id'=>$employeeId], $this->fillHeaders());
    }

    /**
     * Get Currency
     * @param int $currencyId
     *
     * @return object
     */
    public function getCurrency(int $currencyId)
    {
        return $this->client->getRequest(
            $this->appendUrl(UrlConstants::GET_CURRENCY  .  $currencyId), $this->fillHeaders());
    }




    /**
     * Get Company Currency
     * @param mixed $currency
     *
     * @return object
     */
    public function getCompanyCurrency($currency)
    {
        return $this->client->getRequest(
            UrlConstants::GET_CURRENCY . $currency . "?filter=company",
            $this->fillHeaders()
        );
    }

    /**
     * Check attachment
     * @param array $requestBody
     *
     * @return object
     */
    public function checkAttachment(array $requestBody)
    {
        return $this->client->postRequest(
            UrlConstants::CHECK_ATTACHMENT, $requestBody, $this->fillHeaders());
    }

    /**
     * List all Employees
     *
     * @return object
     */
    public function listAllEmployees($limit = null, $page = null, $query = null, $cycle_label = null)
    {
        $query_parameters = [];
        $limit ? $query_parameters['limit'] = $limit : null;
        $page ? $query_parameters['page'] = $page : null;
        $query ? $query_parameters['q'] = $query : null;
        $cycle_label ? $query_parameters['cycle_label'] = $cycle_label : null;
        $parameters = http_build_query($query_parameters);
        $url =  !empty($query_parameters) ? UrlConstants::LIST_ALL_EMPLOYEES . '?' . $parameters : UrlConstants::LIST_ALL_EMPLOYEES;

        return $this->client->getRequest($this->appendUrl($url), $this->fillHeaders());
    }

    public function listAllEmployeesWithoutRelationship(string $column, array $column_array)
    {
        $query_parameters = [];
        $query_parameters['select'] = $column;
        $query_parameters['column_array'] = $column_array;
        $parameters = http_build_query($query_parameters);
        $url =  !empty($query_parameters) ? UrlConstants::LIST_ALL_EMPLOYEES . '?' . $parameters : UrlConstants::LIST_ALL_EMPLOYEES;
        return $this->client->getRequest($this->appendUrl($url), $this->fillHeaders());
    }

    public function getMiniEmployeeByPaygrade(int $paygradeId)
    {
        return $this->client->getRequest(
            UrlConstants::MINI_EMPLOYEE . "?paygrade_id=" . $paygradeId, $this->fillHeaders());
    }

    /**
     * Get full employees
     * @param array $employeeIds
     *
     * @return object
     */
    public function getFullEmployees(array $employeeIds, $limit = null, $page = null, $query = null)
    {
        $query_parameters = [];
        $limit ? $query_parameters['limit'] = $limit : null;
        $page ? $query_parameters['page'] = $page : null;
        $query ? $query_parameters['cycle_label'] = $query : null;
        $parameters = http_build_query($query_parameters);
        $url = !empty($query_parameters) ? UrlConstants::FULL_EMPLOYEES . '?' . $parameters : UrlConstants::FULL_EMPLOYEES;

        return $this->client->postRequest(
            $this->appendUrl($url),
             $employeeIds, $this->fillHeaders()
        );
    }

    /**
     * Save File In Part
     * @param array $requestBody
     *
     * @return object
     */
    public function saveFilePart(array $requestBody)
    {
        $resp = $this->client->postRequest(
            UrlConstants::SAVE_FILE_PART, $requestBody, $this->fillHeaders());
        return $resp;
    }

    /**
     * Save File
     * @param array $requestBody
     *
     * @return object
     */
    public function saveFile(array $requestBody)
    {
        $resp = $this->client->postRequest(
            UrlConstants::SAVE_FILE, $requestBody, $this->fillHeaders());
        return $resp;
    }

    /**
     * Get File
     * @param int $file_id
     *
     * @return object
     */
    public function getFile($file_id, $refeshRequest = false)
    {
        return $this->client->getRequest(
            UrlConstants::GET_FILE . $file_id, $this->fillHeaders($refeshRequest)
        );
    }

    /**
     * Delete File
     * @param string $unique_id
     *
     * @return object
     */

    public function deleteFile($requestBody)
    {
        $resp = $this->client->postRequest(
            UrlConstants::DELETE_FILE, $requestBody, $this->fillHeaders());
        return $resp;
    }

    /**
     * List Departments
     *
     * @return object
     */
    public function listDepartments()
    {
        return $this->client->getRequest(
            $this->appendUrl(UrlConstants::LIST_DEPARTMENTS), $this->fillHeaders()
        );
    }

     /**
     * Get Department
     *
     * @return object
     */
    public function getDepartment($department_id)
    {
        return $this->client->getRequest(
            $this->appendUrl(UrlConstants::GET_DEPARTMENT . $department_id), $this->fillHeaders()
        );
    }

    /**
     * Get client companies
     * @param $client_id
     * @return object
     */
    public function getClientCompanies($client_id = null)
    {
        $client_id = $client_id ?? $this->getClientIdFromHeader();
        return $this->client->getRequest(UrlConstants::GET_CLIENT_COMPANIES . $client_id,
            $this->fillHeaders());
    }


    /**
     * Get Employee Roles
     * @param int $employeeId
     *
     * @return object
     */
    public function getEmployeeRoles(int $employeeId)
    {
        return $this->client->getRequest(UrlConstants::GET_EMPLOYEE_ROLES . $employeeId, $this->fillHeaders());
    }


     /**
     * Get List of Employees in a Role
     * @param int $employeeId
     *
     * @return object
     */
    public function getEmployeesInRole($roleId)
    {
        return $this->client->getRequest(UrlConstants::GET_EMPLOYEES_IN_ROLE . $roleId, $this->fillHeaders());
    }


    /**
     * Get Role Name
     * @param int $employeeId
     *
     * @return object
     */
    public function getRoleName($roleId)
    {
        return $this->client->getRequest(UrlConstants::GET_ROLE_NAME . $roleId, $this->fillHeaders());
    }


    /**
     * Find Job Role Location
     * @param array $payload
     *
     * @return object
     */
    public function findJobRoleLocation(array $payload)
    {
        return $this->client->postRequest(
            UrlConstants::FIND_JOB_ROLE_LOCATION, $payload, $this->fillHeaders());
    }

    public function findCostCenterPaygrade(array $payload)
    {
        return $this->client->postRequest(
            UrlConstants::FIND_COST_CENTER_PAYGRADE, $payload, $this->fillHeaders()
        );
    }

    /**
     * Set Central Cache
     * @param string $key
     * @param mixed $value
     *
     * @return object
     */
    public function setCentralCache($key, $value)
    {
        return $this->client->getRequest(
            UrlConstants::SET_CACHE . "?cache_key=" . $key . "&cache_value=" . $value,
            $this->fillHeaders());
    }


    /**
     * Set an array of cache
     * @param array $data
     * @return mixed
     */
    public function setCentralCacheArray(array $data)
    {
        return $this->client->postRequest(
            UrlConstants::SET_CACHE_ARRAY, $data, $this->fillHeaders());
    }

    /**
     * Get Central Cache
     * @param string $key
     *
     * @return object
     */
    public function getCentralCache($key)
    {
        return $this->client->getRequest(UrlConstants::GET_CACHE . $key, $this->fillHeaders());
    }


    /**
     * check Account Balance
     * @param array [account_number, bank_code]
     * @param mixed $value
     *
     * @return object
     */
    public function checkAccountBalance(array $payload)
    {
        return $this->client->postRequest(
            UrlConstants::CHECK_ACCOUNT_BALANCE, $payload, $this->fillHeaders());
    }


     /**
     * verify Account Details
     * @param array [account_number, bank_code]
     * @param mixed $value
     *
     * @return object
     */
    public function verifyAccountInfo($params)
    {
        return $this->client->getRequest(
            UrlConstants::VERIFY_ACCOUNT_INFO.$params, $this->fillHeaders());
    }


     /**
     * Add company account number to GA
     * @param array [account_number, bank_code]
     * @param mixed $value
     *
     * @return object
     */
    public function createClientAccountGA(array $companyDetails)
    {
        return $this->client->postRequest(
            UrlConstants::CREATE_CLIENT_COMPANY_GA, $companyDetails, $this->fillHeaders());
    }


    /**
     * Update company account number to GA
     * @param array [account_number, bank_code, currency, account_name]
     * @param mixed $value
     *
     * @return object
     */
    public function updateClientAccountGA(array $companyDetails)
    {
        return $this->client->postRequest(
            UrlConstants::UPDATE_CLIENT_COMPANY_GA, $companyDetails, $this->fillHeaders());
    }


    /**
     * Update company account number to GA
     * @param array [
     * 'name', 'amount', 'description', 'reference' => 'required',
          'instructionCount', 'instructions' => 'required|array',
          'payer' => 'required|array',
          'payer.account_number',
          'payer.bank_code' => 'required',
        ]
     * @param mixed $value
     *
     * @return object
     */
    public function payGA(array $disburementDetails)
    {
        return $this->client->postRequest(
            UrlConstants::PAY_GA, $disburementDetails, $this->fillHeaders());
    }

   /**
     * Check if companies belongs to same client.
     * @param $first_company
     * @param $second_company
     * @throws SoaException
     * @return boolean
     */
    public function companiesBelongsToSameClient($first_company, $second_company = null){
        $company = $second_company ? $this->findCompany($second_company) : $this->withRelationship(["client"])->findCompany();
        if($company->status != StatusCode::OK){
            throw new SoaException($company->message, $company->status);
        }
        $client_id = json_decode(json_decode($company->data->client->info))->app_id;
        $companies = $this->getClientCompanies($client_id);
        if($companies->status != StatusCode::OK){
            throw new SoaException($companies->message, $companies->status);
        }
        return in_array($first_company, $companies->data->ids);
    }



    /**
     * Get Company Account details
     * @param mixed $id - the company_account id
     *
     * @return object
     */
    public function getCompanyAccountDetails($id)
    {
        return $this->client->getRequest(
            UrlConstants::COMPANY_ACCOUNT_DETAILS.$id, $this->fillHeaders());
    }



    /**
     * Generate Transaction reference
     * @param mixed $id - the company_account id
     *
     * @return object
     */
    public function generatePaymentReference()
    {
        return $this->client->getRequest(
            UrlConstants::GENERATE_PAYMENT_REFERENCE, $this->fillHeaders());
    }


    /**
     * getClientFromAppId
     * @param mixed $app_id - the company_account id
     *
     * @return object
     */
    public function getClientFromAppId(string $app_id)
    {
        return $this->client->getRequest(UrlConstants::CLIENT_FROM_APP_ID.$app_id, $this->fillHeaders());
    }

    public function isSme(string $app_id)
    {
        $client_data = optional($this->client->getRequest(UrlConstants::CLIENT_FROM_APP_ID.$app_id, $this->fillHeaders()))->data;
            if ($client_data)
            $client = json_decode(json_decode($client_data->info));
            $is_sme = optional($client)->is_sme ?? false;
            return $is_sme;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getClientWithIndex(int $id)
    {
        return $this->client->getRequest(UrlConstants::CLIENT_FROM_INDEX_ID.$id, $this->fillHeaders());
    }

    public function getEmployeeByEmployeeCode(array $body)
    {
        return $this->client->postRequest(UrlConstants::EMPLOYEE_FROM_EMPLOYEE_CODE, $body);
    }

    /**
     *
     * @param  int    $id  - auth id
     * @return mixed
     */
    public function getEmployeeIdFromAuthId(int $id)
    {
      return $this->client->getRequest(UrlConstants::GET_EMPLOYEE_ID_FROM_AUTH_ID.$id, $this->fillHeaders());
    }

    /**
     *
     * @param  int    $id  - auth id
     * @return mixed
     */
    public function getEmployeeIdFromAuthIdNoAuth(int $id)
    {
      return $this->client->getRequest(UrlConstants::GET_EMPLOYEE_ID_FROM_AUTH_ID_NO_AUTH.$id, $this->fillHeaders());
    }

    /**
     * Get Country Information
     * @param mixed $country_code - the country_code
     *
     * @return object
     */
    public function getCountry(string $country_code)
    {
        return $this->client->getRequest(UrlConstants::GET_COUNTRY . $country_code, $this->fillHeaders());
    }

    public function currentCompanyHasToggleFeature(string $feature, array $headers = [])
    {
        $data = ['name' => $feature];
        $requestHeaders = $headers ?: $this->fillHeaders();
        $response = $this->findClientToggleFeature($data, $headers);
        return ($response->status == StatusCode::OK && optional($response->data)->show_feature) ? true: false;
    }

    protected function findClientToggleFeature(array $data, $headers = [])
    {
        $url = UrlConstants::FIND_CLIENT_TOGGLE_FEATURE.'?'.http_build_query($data);
        return $this->client->getRequest($url, $headers);
    }

    public function getBreezeToken(): string
    {
        $url = env("BREEZE_BASE_URL").'/api/v1/auth/client/request-token';
        $data = [
            'client_id' => env('BREEZE_CLIENT_ID'),
            'client_secret' => env('BREEZE_CLIENT_SECRET')
        ];

        $response = $this->client->externalPostRequest($url, $data, []);

        if (empty($response) || !property_exists($response, 'access_token')) {
            info('Breeze Authenication Error Response => '.json_encode($response));
            throw new BreezeAuthenticationException($response->message ?? 'Error generating breeze token', 1);
        }

        return $response->access_token;
    }
}
