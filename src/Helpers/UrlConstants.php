<?php

namespace SeamlessHr\SoaRequest\Helpers;

class UrlConstants
{
    const GET_CACHE = "/api/v1/cache/get-cache?cache_key=";

    const GET_SERVICE_CACHE = "/api/v1/cache/get-service-urls";

    const SET_CACHE = "/api/v1/cache/set-cache";

     const CHECK_ACCOUNT_BALANCE = "/api/v1/payment/check-balance";

    const VERIFY_ACCOUNT_INFO = "/api/v1/payment/verify-account-number";

    const START_APPROVAL = "/api/v1/approval/start";

    const APPROVAL_STATUS = "/api/v1/approval/get-current-status";

    const FIND_COMPANY = "/api/v1/organisation/find-company/";

    const DELETE_APPROVAL = "/api/v1/approval/delete-approval-details";

    const DELETE_APPROVAL_V2 = "/api/v2/approval/delete-approval-details";

    const SEARCH_EMPLOYEES = "/api/v1/employee/search-employee?q=";

    const COMPANY_BASE_CURRENCY = "/api/v1/organisation/get-base-currency";

    const MINI_EMPLOYEE = "/api/v1/employee/get-mini-employee";

    const ACTIVE_EMPLOYEES = "/api/v1/employee/get-active-employees";

    const SUBMIT_OTP_CODE = "/api/v1/notification/submit-otp";

    const FULL_EMPLOYEE = "/api/v1/employee/get-full-employee-service?id=";

    const GET_CURRENCY = "/api/v1/organisation/get-currency?currency_id=";

    const GET_CURRENCIES = "/api/v1/organisation/list-currencies";

    const GET_BASE_CURRENCIES = "/api/v1/organisation/get-base-currency";

    const CHECK_ATTACHMENT = "/api/v1/employee/check-attachment";

    const LIST_ALL_EMPLOYEES = "/api/v1/employee/list-all-employees-service";

    const FULL_EMPLOYEES = "/api/v1/employee/get-full-employee";

    const SAVE_FILE = "/api/v1/file/save/file";

    const SAVE_FILE_PART = "/api/v1/file/save/file-part";

    const GET_FILE = "/api/v1/file/get/file?id=";

    const DELETE_FILE = "/api/v1/file/delete/file";

    const LIST_DEPARTMENTS = "/api/v1/organisation/list-departments";

    const GET_DEPARTMENT = "/api/v1/organisation/view-department?id=";

    const RECURRING_PAYROLL_UPDATE = "/api/v2/payroll/list-recurring-payroll-updates";

    const PAYROLL_UPDATE = "/api/v1/payroll/list-all-payroll-updates";

    const PAYROLL_CYCLE = "/api/v1/payroll/list-payroll-cycles";

    const LIST_PAY_GRADE = "/api/v1/payroll/list-pay-grades";

    const LIST_COST_CENTRES = "/api/v1/payroll/list-cost-centres";

    const LIST_ALLOWANCES = "/api/v1/payroll/list-allowances";

    const LIST_DEDUCTIONS = "/api/v1/payroll/list-statutory-deductions";

    const VIEW_STATE = "/api/v1/organisation/view-state?id=";

    const GET_CLIENT_COMPANIES = "/api/v1/organisation/find-client-companies?client_id=";

    const CREATE_EMPLOYEE = "/api/v1/auth/create/employee";

    const UPDATE_EMPLOYEE = "/api/v1/auth/update/employee";

    const GET_EMPLOYEE_ROLES = "/api/v1/auth/get-employee-roles-names?employee_id=";

    const FIND_COST_CENTER_PAYGRADE = "/api/v1/payroll/find-costcenter-paygrade";

    const FIND_JOB_ROLE_LOCATION = "/api/v1/organisation/find-jobrole-location";

    const GET_APPROVAL_LIST = "/api/v1/approval/get-approval-list";

    const GET_EMPLOYEES_IN_ROLE = "/api/v1/auth/get-employees-in-role?id=";

    const CREATE_CLIENT_COMPANY_GA = "/api/v1/payment/create-client-account";

    const UPDATE_CLIENT_COMPANY_GA = "/api/v1/payment/update-client-account";

    const PAY_GA = "/api/v1/payment/pay";

    const SET_CACHE_ARRAY = "/api/v1/cache/set-cache-array";

    const COMPANY_ACCOUNT_DETAILS = "/api/v1/organisation/view-company-bank-account?id=";

    const GENERATE_PAYMENT_REFERENCE = "/api/v1/payment/generate-reference";

    const CLIENT_FROM_APP_ID = "/api/v1/client/";

    const CLIENT_FROM_INDEX_ID = "/api/v1/client/client-id/";

    const EMPLOYEE_FROM_EMPLOYEE_CODE = "/api/v1/employee/get-employee-by-code";

    const GET_ROLE_NAME = "/api/v1/auth/get-role-name?role_ids=";

    const GET_EMPLOYEE_ID_FROM_AUTH_ID = "/api/v1/auth/get-employee-id-from-auth-id?id=";

    const GET_EMPLOYEE_ID_FROM_AUTH_ID_NO_AUTH = "/api/v1/auth/get-employee-id-from-auth-id-service?id=";

    const GET_COUNTRY = "/api/v1/organisation/get-country?country_code=";

    const FIND_CLIENT_TOGGLE_FEATURE = "/api/v1/client/find-feature-toggle";

}
