<?php

namespace SeamlessHr\SoaRequest\Helpers;

use Illuminate\Http\Request;
use SeamlessHr\SoaRequest\Contracts\ClientInterface;

/**
 * Base Class For All Soa Commons
 */
abstract class BaseCommon
{
    /**
     * @var SeamlessHr\SoaRequest\Contracts\ClientInterface
     */
    protected $client;

    /**
     * @var Illuminate\Http\Request
     */

    protected $request, $headers, $with;

    public function __construct(ClientInterface $client, Request $request)
    {
        $this->client = $client;
        $this->request = $request;
        $this->headers = $this->makeHeaders();
    }

    /**
     * Fill Header
     * @return array
     */
    protected function fillHeaders($refreshRequest = false)
    {
        return $refreshRequest ? $this->makeHeaders() : $this->headers;
    }

    public function makeBase64()
    {
        $this->client->setBase64();
        return $this;
    }

    public function base64Response()
    {
        $this->client->sendBase64Response();
        return $this;
    }

    /**
     * Fill Header
     * @return array
     */
    protected function makeHeaders()
    {
        return [
            "company-id: " . $this->request->header('company-id'),
            "client-id: " . $this->request->header('client-id'),
            "Authorization: ".$this->request->header('Authorization')
        ];
    }

    public function withRelationship($relationships)
    {
        $this->with = null;
        foreach ($relationships as $key => $relationship){
            $this->with = $key > 0 ? $this->with .'&with[]='.$relationship : $this->with .'with[]='.$relationship ;
        }
        return $this;
    }

    /** Transform the url with with relationships
     * @param $url
     * @return string
     */
    protected function appendUrl($url)
    {
        $with = (string)$this->with;
        return !empty($with) ? ((strpos($url, '?') !== false) ? $url.'&'.$with : $url.'?'.$with) : $url;
    }
    /**
     * Add array headers
     * @param array $headers
     * @return SoaCommon
     */
    public function withHeaders(array $headers)
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * Get client id from Headers
     * @return int $clientId
     */
    protected function getClientIdFromHeader()
    {
        return $this->request->header("client-id");
    }

    /**
     * Fill Company Header
     * @return array
     */
    protected function fillCompanyHeader()
    {
        return [$this->fillHeaders()[0]];
    }

    /**
     * Get company id from header
     * @return int
     */
    protected function companyId()
    {
        $company_id = $this->request->header('company-id');
        if (is_null($company_id)){
            $headers = $this->headers;
            foreach ($headers as $header){
                if (strpos($header, 'company-id') !== false){
                    $company_id = trim(explode(':', $header)[1]);
                    break;
                }
            }
        }
        return $company_id;
    }
}
