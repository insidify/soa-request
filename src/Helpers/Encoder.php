<?php

namespace SeamlessHr\SoaRequest\Helpers;

use Spatie\ArrayToXml\ArrayToXml;

/**
 * Encodes to base64
 */

 class Encoder
 {
    /**
     * @var mixed $encodable
     */
    protected $encodable;

    /**
     * Construct Encoder
     * @var mixed $encodable
     */
    public function __construct($encodable)
    {
        $this->encodable = $encodable;
    }

    /**
     * Encoder Factory
     * @var mixed $encoder
     * @return Encoder
     */
    public static function factory($encodable)
    {
        return new self($encodable);
    }

    /**
     * Encode to base64
     * @return string $encodable
     */
    public function __invoke()
    {
        $this->encodable = serialize($this->encodable);
        return base64_encode($this->encodable);
    }

    /**
     * Encode to XML
     * @param string $customRootName
     */
    public function arrayToXml(string $customRootName)
    {
        return ArrayToXml::convert($this->encodable, $customRootName);
    }
 }