<?php

namespace SeamlessHr\SoaRequest\Helpers;

/**
 * Decodes base64
 */

 class Decoder
 {
    /**
     * @var mixed $decodable
     */
    protected $decodable;

    /**
     * Construct Encoder
     * @var mixed $decodable
     */
    public function __construct($decodable)
    {
        $this->decodable = $decodable;
    }

    /**
     * Decoder Factory
     * @var mixed $decodable
     * @return Decoder
     */
    public static function factory($decodable)
    {
        return new self($decodable);
    }

    /**
     * Decode base64
     * @return string $decodable
     */
    public function __invoke()
    {
        $this->decodable = base64_decode($this->decodable);
        return unserialize($this->decodable);
    }

    /**
     * Decode Xml To Array
     * @param string $xml
     * @return array
     */
    public function xmlToArray()
    {
        $xml = json_encode(simplexml_load_string($this->decodable));
        return json_decode($xml,true);
    }
 }