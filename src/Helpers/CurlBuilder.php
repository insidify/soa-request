<?php

namespace SeamlessHr\SoaRequest\Helpers;

use Ixudra\Curl\Builder;

class CurlBuilder extends Builder
{

    /**
     * Add XmlRequest to Package Options
     * @param array $xmlPackageOption
     */
    protected $xmlPackageOption = [
        "asXmlRequest" => false,
        "withXmlRoot" => "root",
        "decodeXmlResponse" => false
    ];

    /**
     * Add Xml to Package Options
     */
    public function __construct()
    {
        $this->packageOptions += $this->xmlPackageOption;
    }

    /**
     * Configure the package to encode the request data to xml before sending it to the server
     *
     * @return Builder
     */
    public function asXmlRequest()
    {
        return $this->withPackageOption( 'asXmlRequest', true );
    }

    /**
     * Configure the xml request to use custom root when encoding
     *
     * @return Builder
     */
    public function withXmlRoot(string $root)
    {
        return $this->withPackageOption( 'withXmlRoot', $root );
    }

    /**
     * Decode xml response
     *
     * @return Builder
     */
    public function decodeXmlResponse()
    {
        return $this->withPackageOption( 'decodeXmlResponse', true );
    }

    /**
     * Add POST parameters to the curlOptions array
     */
    protected function setPostParameters()
    {
        $this->curlOptions[ 'POST' ] = true;

        $parameters = $this->packageOptions[ 'data' ];

        if( $this->packageOptions[ 'asXmlRequest' ] ) {
            $parameters = Encoder::factory($parameters)->arrayToXml(
                $this->packageOptions["withXmlRoot"]
            );
        }

        $this->curlOptions[ 'POSTFIELDS' ] = $parameters;
    }

    /**
     * Send the request
     *
     * @return mixed
     */
    protected function send()
    {
        // Add XML header if necessary
        if( $this->packageOptions[ 'asXmlRequest' ] ) {
            $this->withHeader( 'Content-Type: application/xml' );
        }

        return parent::send();
    }

    /**
     * @param   mixed $content          Content of the request
     * @param   array $responseData     Additional response information
     * @param   string $header          Response header string
     * @return mixed
     */
    protected function returnResponse($content, array $responseData = array(), $header = null)
    {
        if( $this->packageOptions[ 'decodeXmlResponse' ]) {
            $content = Decoder::factory($content)->xmlToArray();
        }
        
        return parent::returnResponse($content, $responseData, $header);
    }

}