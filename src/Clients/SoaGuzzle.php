<?php

namespace SeamlessHr\SoaRequest\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\TransferStats;
use SeamlessHR\SoaBulkUpload\Enums\StatusEnum;
use SeamlessHr\SoaRequest\Helpers\Decoder;
use SeamlessHr\SoaRequest\Helpers\Encoder;
use SeamlessHr\SoaRequest\Traits\Appendable;
use SeamlessHR\SoaUtils\Interfaces\StatusCode;
use SeamlessHr\SoaRequest\Contracts\ClientInterface;

class SoaGuzzle implements ClientInterface
{
    use Appendable;
    /**
     * @var GuzzleHttp\Client
     *
     */
    protected $client;
    private $requestTime = 0;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }


    /**
     * @param string $url
     * @param $request
     * @param array $headers
     * @return mixed
     */
    public function postRequest($url, $request, $headers = [])
    {
        $headers['caller-secret'] = bcrypt(config("soautils.caller_secret"));
        try{
            $url = $this->appendToUrl($url);
            $headers = $this->parseHeaders($headers);
            $body = $this->prepareBody($request);
            $guzzleService = $this->client->post($url, ["form_params" => $body, "headers" => $headers]);
            return $this->formatResponse($guzzleService);
        }
        catch (RequestException $e){
            return $this->formatError($e);
        }
    }

    /**
     * @param string $url
     * @param $request
     * @param array $headers
     * @return mixed
     */
    public function patchRequest($url, $request, $headers = [])
    {
        $headers['caller-secret'] = bcrypt(config("soautils.caller_secret"));
        try{
            $url = $this->appendToUrl($url);
            $headers = $this->parseHeaders($headers);
            $body = $this->prepareBody($request);
            $guzzleService = $this->client->request('PATCH', $url, ["form_params" => $body], ["headers" => $headers]);
            return $this->formatResponse($guzzleService);
        }
        catch (RequestException $e){
            return $this->formatError($e);
        }
    }

    /**
     * Perform an external patch request without adding base url
     * @param $url
     * @param $requestBody
     * @param $headers
     */
    public function externalPatchRequest($url, $request, $headers = [])
    {
        $headers['caller-secret'] = bcrypt(config("soautils.caller_secret"));
        try{
            $headers = $this->parseHeaders($headers);
            $body = $this->prepareBody($request);
            $guzzleService = $this->client->request('PATCH', $url, ["form_params" => $body], ["headers" => $headers]);
            return json_decode($guzzleService->getBody());
        }
        catch (RequestException $e){
            return $e->getResponse();
        }
    }

    /**
     * @param string $url
     * @param array $headers
     * @return mixed
     */
    public function getRequest($url, $headers = [])
    {
        $headers['caller-secret'] = bcrypt(config("soautils.caller_secret"));
        try{
            $url = $this->appendToUrl($url);
            $headers = $this->parseHeaders($headers);
            $guzzleService = $this->client->request('GET', $url, ["headers" => $headers]);

            return $this->formatResponse($guzzleService);
        }
        catch (RequestException $e){
            return $this->formatError($e);
        }
    }

    private function formatError($error) : \stdClass
    {
        $response = new \stdClass();
        $response->status = $error->getCode();
        $response->data = [];

        if ($error->getCode() == StatusCode::UNAUTHORIZED) {
            $response->message = "Token is expired";
        }

        return $response;
    }

    private function formatResponse($guzzleService)
    {
        $resp = json_decode($guzzleService->getBody());

        if (isset($resp->data) && is_string($resp->data)) {
            $resp->data = json_decode($resp->data);
        }

        return $resp;
    }

    /**
     * Parse heaaders to fit guzzle format
     * @param array $headers
     *
     * @return array $headers
     */
    protected function parseHeaders(array $headers)
    {
        $headerInfo = [];

        foreach ($headers as $header) {

            $data = explode(':', $header);

            if (($value = $data[1] ?? '') != '') {

                $decodedValue = json_decode($value);

                $value = $decodedValue ?? $value;

                $headerInfo[$data[0]] = trim($value);
            }
        }

        return $headerInfo;
    }

    /**
     * Perform an external post request without adding base url
     * @param $url
     * @param $requestBody
     * @param $headers
     */
    public function externalPostRequest($url, $request, $headers = [])
    {
        $headers['caller-secret'] = bcrypt(config("soautils.caller_secret"));
        try{
            $headers = $this->parseHeaders($headers);

            $body = $this->prepareBody($request);

            $guzzleService = $this->client->post( $url, ["json" => $body, "headers" => $headers, 'on_stats' => $this->setRequestStats()]);

            return $guzzleService;
        }
        catch (RequestException $e){
            return $e->getResponse();
        }
    }

    private function setRequestStats() : \Closure
    {
        return function (TransferStats $stats){
            $this->requestTime = $stats->getTransferTime();
        };
    }

    public function getRequestDuration() : float
    {
        return $this->requestTime;
    }

    /**
     * Perform an external get request without adding base url
     * @param $url
     * @param $requestBody
     * @param $headers
     */
    public function externalGetRequest($url, $headers = [])
    {
        $headers['caller-secret'] = bcrypt(config("soautils.caller_secret"));
        try{

            $headers = $this->parseHeaders($headers);

            $guzzleService = $this->client->get($url, ["headers" => $headers, 'on_stats' => $this->setRequestStats()]);


            return $guzzleService;
        }
        catch (RequestException $e){
            return $e->getResponse();
        }
    }

    public function uploadFile($url, $filePath, $extension, $title, $header, $mime = null)
    {
        $headers['caller-secret'] = bcrypt(config("soautils.caller_secret"));
        //$curlService = new \Ixudra\Curl\CurlService();
        $url = $this->appendToUrl($url);

        $mime = $mime ?? ($extension == 'csv'
            ? "text/csv" : "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        // $micro_time = microtime()."";
        // $short_numeric_name = str_replace(' ','',$micro_time);
        // $short_numeric_name = str_replace('.','',$short_numeric_name);
        $curlFile = new \CURLFile($filePath, $mime, $title.'.'.$extension);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'file' => $curlFile,
            'type' => 'document'
        ]);

        $result = curl_exec($ch);
        
        if ($result === false) {
            echo 'upload - FAILED' . PHP_EOL;
        }
        return json_decode($result);
    }
    
    public function isSuccessFul(int $statusCode) : bool
    {
        return in_array($statusCode, [StatusCode::OK, StatusCode::CREATED, StatusCode::UPDATED]);
    }
}