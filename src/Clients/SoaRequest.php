<?php

namespace SeamlessHr\SoaRequest\Clients;

use Illuminate\Support\Str;
use SeamlessHr\SoaRequest\Traits\Appendable;
use SeamlessHR\SoaUtils\Interfaces\StatusCode;
use SeamlessHr\SoaRequest\Contracts\ClientInterface;

class SoaRequest implements ClientInterface
{
    use Appendable;

    /**
     * This sets default parameters for POST Request
     * @param String $url: this is the url of the service you trying to go to
     * @param Request $request this is the request object
     * @param Array $headers
     * @return  Response $response this is the refined request object
     */

    public function postRequest($url, $request, $headers=[])
    {
        $headers['caller-secret'] = bcrypt(config("soautils.caller_secret"));
        $curlService = new \Ixudra\Curl\CurlService();
        $url = $this->appendToUrl($url);

        try {
            $response = $curlService->to($url)
                ->withHeaders($headers)
                ->withData($this->prepareBody($request))
                ->post();
            $response = json_decode($this->checkIfBase64($response));
            if(isset($response->content) &&  !is_null($response->content)){
                return $this->decodeBase64Response($response->content);
            }else{
                return $this->decodeBase64Response($response);
            }
        } catch (Exception $e) {
            return $response;
        }


    }

    /**
     * Perform a patch request
     * @param string $url
     * @param array $requestBody
     * @param array $headers
     */
    public function patchRequest($url, $requestBody, $headers = [])
    {
        $curlService = new \Ixudra\Curl\CurlService();
        $url = $this->appendToUrl($url);
        $headers['caller-secret'] = bcrypt(config("soautils.caller_secret"));
        try {
            $response = $curlService->to($url)
                ->withHeaders($headers)
                ->withData($this->prepareBody($requestBody))
                ->patch();

            $response = json_decode($this->checkIfBase64($response));

            if(isset($response->content) &&  !is_null($response->content)){
                return $this->decodeBase64Response($response->content);
            }else{
                return $this->decodeBase64Response($response);
            }
        } catch (Exception $e) {
            return $response;
        }
    }

    /**
     * Perform an external patch request
     * @param string $url
     * @param array $requestBody
     * @param array $headers
     */
    public function externalPatchRequest($url, $requestBody, $headers = [])
    {
        $curlService = new \Ixudra\Curl\CurlService();
        $headers['caller-secret'] = bcrypt(config("soautils.caller_secret"));
        try {
            $curlService = $curlService->to($url)
                ->withHeaders($headers)
                ->withData($this->prepareBody($requestBody));

            if (Str::contains($url, ['https://hrms.nibss-plc.com.ng', 'https://hris.cardinalstone.com'])) {
                $curlService = $curlService->withOption('SSL_VERIFYPEER', false);
            }

            $response = $curlService->patch();

            $response = json_decode($response);

            if(isset($response->content) &&  !is_null($response->content)){
                return $response->content;
            }else{
                return $response;
            }
        } catch (Exception $e) {
            return $response;
        }
    }

    /**
     * This sets default parameters for POST Request
     * @param String $url: this is the url of the service you trying to go to
     * @param Request $request this is the request object
     * @param Array $headers
     * @param Array $file: file to send with the request
     * @return  Response $response this is the refined request object
     */
    public function externalPostRequest($url, $request, $headers=[], $file=[])
    {

        $curlService = new \Ixudra\Curl\CurlService();
        $headers['caller-secret'] = bcrypt(config("soautils.caller_secret"));
        $headers = array_merge($this->headers, $headers);

        try {
            $curlService = $curlService->to($url)
                ->withHeaders($headers)
                ->withTimeout(config('soarequest.external_request_timeout'))
                ->withData($request)
                ->returnResponseObject();

            if (Str::contains($url, ['https://hrms.nibss-plc.com.ng', 'https://hris.cardinalstone.com'])) {
                $curlService = $curlService->withOption('SSL_VERIFYPEER', false);
            }

            // Add the file if provided
            if ($file && isset($file['key'], $file['extension'], $file['path'], $file['name'])) {
                $fileType = $file['extension'] == 'xlsx' ? 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' : 'text/csv';
                $curlService = $curlService->withFile($file['key'], $file['path'], $fileType, $file['name']);
            } else {
                $curlService = $curlService->asJson();
            }

            $response = $curlService->post();

            if(!is_object($response))
                $response = json_decode($response);

            if(isset($response->content) &&  !is_null($response->content)){
                return $response->content;
            }else{
                return $response;
            }
        } catch (Exception $e) {
            return $response;;
        }


    }



    /**
     * This sets default parameters for GET Request
     * @param String $url: this is the url of the service you trying to go to
     * @param Request $request this is the request object
     * @param Array $headers
     * @return  Response $response this is the refined request object
     */
    public function getRequest($url, $headers=[])
    {
        $curlService = new \Ixudra\Curl\CurlService();
        $headers['caller-secret'] = bcrypt(config("soautils.caller_secret"));
        $url = $this->appendToUrl($url);

        try {
            $response = $curlService->to($url)
                ->withHeaders( $headers )
                ->get();

            $response = json_decode($this->checkIfBase64($response));

            if(isset($response->content) &&  !is_null($response->content)){
                return $this->decodeBase64Response($response->content);
            }else{
                return $this->decodeBase64Response($response);
            }
        } catch (Exception $e) {
            return $response;
        }

    }

    /**
     * This sets default parameters for GET Request
     * @param String $url: this is the url of the service you trying to go to
     * @param Request $request this is the request object
     * @param Array $headers
     * @return  Response $response this is the refined request object
     */
    public function externalGetRequest($url, $headers=[])
    {
        $curlService = new \Ixudra\Curl\CurlService();
        $headers['caller-secret'] = bcrypt(config("soautils.caller_secret"));
        $headers = array_merge($this->headers, $headers);

        try {
            $curlService = $curlService->to($url)
                ->withHeaders($headers)
                ->withTimeout(config('soarequest.external_request_timeout'));

            if (Str::contains($url, ['https://hrms.nibss-plc.com.ng', 'https://hris.cardinalstone.com'])) {
                $curlService = $curlService->withOption('SSL_VERIFYPEER', false);
            }

            $response = $curlService->get();

            if(!is_object($response))
                $response = json_decode($response);

            if(isset($response->content) &&  !is_null($response->content)){
                return $response->content;
            }else{
                return $response;
            }
        } catch (Exception $e) {
            return $response;;
        }

    }


    public function uploadFile($url, $filePath, $extension, $title, $header, $mime = null)
    {
        $headers['caller-secret'] = bcrypt(config("soautils.caller_secret"));
        //$curlService = new \Ixudra\Curl\CurlService();
        $url = $this->appendToUrl($url);

        $mime = $mime ?? ($extension == 'csv'
            ? "text/csv" : "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        // $micro_time = microtime()."";
        // $short_numeric_name = str_replace(' ','',$micro_time);
        // $short_numeric_name = str_replace('.','',$short_numeric_name);
        $curlFile = new \CURLFile($filePath, $mime, $title.'.'.$extension);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'file' => $curlFile,
            'type' => 'document'
        ]);

        $result = curl_exec($ch);
        
        if ($result === false) {
            echo 'upload - FAILED' . PHP_EOL;
        }
        return json_decode($result);
    }

    /**
     * Make an xml request
     * @param string $url
     * @param array $headers
     * @param string $method
     * @param array $body
     *
     * @return string $response
     *
     */
    public function xmlRequest($url, $body = [], $root = "root", $headers = [])
    {
        $curlService = new CurlService();
        $headers['caller-secret'] = bcrypt(config("soautils.caller_secret"));
        $request = $curlService->to($url)
            ->withHeaders($headers)
            ->asXmlRequest()
            ->withXmlRoot($root)
            ->decodeXmlResponse()
            ->returnResponseObject()
            ->withData($body);

        try {
            return $request->post();
        } catch (Exception $e) {
            return $response;
        }

    }
}