<?php

namespace SeamlessHr\SoaRequest\Clients;


use Ixudra\Curl\CurlService as Ixudra;
use SeamlessHr\SoaRequest\Helpers\CurlBuilder;

class CurlService extends Ixudra
{
    /**
     * @param $url string   The URL to which the request is to be sent
     * @return \Ixudra\Curl\Builder
     */
    public function to($url)
    {
        $builder = new CurlBuilder();

        return $builder->to($url);
    }
}